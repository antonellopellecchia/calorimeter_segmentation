from random import random

def c_sample_point(dist_array, max_y):
	#print('sampling point...')
	x = random()*len(dist_array)
	y = random()*max_y
	while y > dist_array[int(x)]:
		x = random()*len(dist_array)
		y = random()*max_y
	#print('done', x, y, dist_array[int(x)])
	return x