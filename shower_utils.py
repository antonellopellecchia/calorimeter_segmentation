import math, numpy
from convolution_utils import Distribution
from scipy.interpolate import interp1d
from scipy.integrate import quad

c = 3.e8
#v = 2/41*c
v = 1/3*c
radiation_length = 9.2e-3
calorimeter_length = 25*radiation_length
L = calorimeter_length

def n_rad_lengths(t):
	# Position a photon has been produced
	# if it arrives on the photosensor at time t,
	# in number of radiation lengths
	return (t - calorimeter_length/v)/(1/c-1/v)/radiation_length

def shower_func(x, beta=1):
	# Greisen shower shape parametrization
	# x is n. of radiation lengths
	if x == 0: return 0
	s = 3*x/(x+2*beta)
	return 0.135/math.sqrt(beta)*math.exp(x*(1-1.5*math.log(s)))

def shower_func_normalized(x, beta=1):
	return shower_func(x, beta)/shower_func_norm

def shower_func_t(t, beta=1):
	# Shower shape at calorimeter end as function of time
	if t<calorimeter_length/c or t>calorimeter_length/v: return 0
	return shower_func(n_rad_lengths(t), beta)

def shower_func_t_normalized(x, beta=1):
	return shower_func_t(x, beta)/shower_func_t_norm

def scint_light_func(t):
	# scintillation light distribution (Knoll)
	# sum of rising and falling exponential
	# unused (replaced by shower shape)
	tau = 3*1.7e-9 # in ns, decay time
	tau1 = 3*.2e-9 # rising time
	I = 1e3
	return I*(math.exp(-t/tau)-math.exp(-t/tau1))