## Script description

### convolution_tests.py
This file calculates the convolutions (via a Monte Carlo algorithm) of some pairs of distributions (gaussian, exponential, laplace, uniform) and verifies the cumulant additivity. For each pair of distributions, the Monte Carlo calculation is carried out for a different number of steps ranging from 100 to 10^4. The relative discrepancies between the cumulants are plotted in a image stored in the folder `graphs/convolution_tests`. For example, the file `gaus_0.png` contains the discrepancies for the order 1 cumulant, running as the sampling number increases.

Usage: `python3 convolution_tests.py`
Requires: `matplotlib, tqdm`

### pmt_signal.py
This file reads the single-photon response shape of a photomultiplier from file pmt_spr_796.csv (data from [10.1016/0029-554X(72)90489-2](https://www.sciencedirect.com/science/article/pii/0029554X72904892)), then convolutes it with a shower shape distribution (Greiser parametrization) to get the PMT response and it compares the cumulants of the resulting distributions with the sums of the cumulants for the original distributions, plotting the relative discrepancies in an image in the folder `graphs/pmt_signal`. Finally, it builds the original light distribution from the cumulants calculating an inverse Fourier transformation (zombie).

Usage: `python3 pmt_signal.py 100000 plot`
Requires: `matplotlib, tqdm, scipy, numpy`

### convolution_utils.py
Utility classes and functions used by both scripts above.

### shower_utils.py
Functions describing a shower shape for a PWO omogeneous calorimeter in function of radiation length or time.