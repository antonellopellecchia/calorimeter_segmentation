import math, random, cmath
import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm
from scipy import special, integrate
from scipy.interpolate import interp1d

from cfunctions import c_sample_point

import seaborn as sns
sns.set()
sns.set_context("notebook")
sns.set_style("darkgrid")

max_order = 8

def read_csv(fname):
	l = list()
	f = open(fname, 'r')
	line = f.readline()
	count = 0
	while line!='':
		count += 1
		l.append(line)
		line = f.readline()
	f.close()
	data = list()
	for line in l:
		pair_str = line.split(', ')
		pair_float = [float(el) for el in pair_str]
		data.append(pair_float)
	x = [pair[0] for pair in data]
	y = [pair[1] for pair in data]
	return x, y, count

def transpose_matrix(m):
	return list(map(list, zip(*m)))

#def spr_func(x):
#	# single photon response
#	j = 0
#	while times[j+1]<x: j+=1
#	return spr[j]+(x-times[j])*(spr[j+1]-spr[j])/(times[j+1]-times[j])

def ift(x):
	# inverse fourier transform (discrete)
	N = len(x)
	return [sum([x[k]*cmath.exp(2*cmath.pi*1j*n*k/N)/N for k in range(N)]) for n in range(N)]

times, spr, nstep = read_csv('./pmt_spr_796.csv')

class Distribution:
	def __init__(self, f, x, normalized=False, y=None, error=None, **args):
		if str(type(f)) == "<class 'function'>":
			self.function = f
			self.dist_array = None
			self.args = args
			self.discretize()
		elif str(type(f)) == "<class 'list'>" or str(type(f)) == "<class 'numpy.ndarray'>":
			if y is not None:
				self.dist_array = Distribution.interpolate(f, x, y)
			else: self.dist_array = f
			self.function = None
		self.normalized = normalized
		self.x = x
		self.error = [0]*len(x) if error is None else error

	def __sub__(self, dist):
		if not np.array_equal(self.x,dist.x):
			raise Exception('Distributions must have same x value sets')
		diff = Distribution(
				f = np.array([(self.dist_array[j]-dist.dist_array[j])/self.dist_array[j]
					if self.dist_array[j]!=0 else 0 for j in range(len(self.x))]),
				x = np.array(self.x),
				error = np.array([(self.error[j]+dist.error[j])/self.dist_array[j]
					if self.dist_array[j]!=0 else 0 for j in range(len(self.x))])
			)
		return diff

	def discretize(self):
		if self.dist_array is not None: return
		self.dist_array = [self.function(x_k, **self.args) for x_k in self.x]

	def normalize(self):
		self.discretize()
		if self.normalized: return
		s = self.moment(0)[0]
		self.dist_array = np.array(self.dist_array)/s
		self.error = np.array(self.error)/s
		self.normalized = True

	def moment(self, order):
		if self.dist_array is None: self.discretize()
		x = [self.dist_array[k]*self.x[k]**order*abs(self.x[k]-self.x[k-1])
			for k in range(1, len(self.x))]
		e = [self.error[k]*self.x[k]**order*abs(self.x[k]-self.x[k-1])
			for k in range(1, len(self.x))]
		return sum(x), sum(e)

	def moments(self, order=max_order):
		m = np.array([self.moment(i) for i in range(order)])
		x = np.array([el[0] for el in m])
		e = np.array([el[1] for el in m])
		return x, e

	def cumulants(self, order=max_order):
		return Distribution.cumulants_from_moments(self.moments(order))

	def plot(self, path='', title='', xlabel='', show=False):
		if self.dist_array is None: self.discretize()
		fig, ax = plt.subplots()
		ax.set_title(title)
		ax.set_xlabel(xlabel)
		ax.plot(self.x, self.dist_array)
		if show: plt.show()
		if path!='': fig.savefig(path, dpi=150)
		plt.close(fig)

	def sample_point(self):
		# Slow, use c_sample_point from cfunctions
		x = random.random()*len(self.dist_array)
		y = random.random()
		while y > self.dist_array[int(x)]:
			x = random.random()*len(self.dist_array)
			y = random.random()
		return x

	def sample(self, mc_npoints):
		result_dist = [0]*self.length()
		for k in tqdm(range(mc_npoints)):
			point = self.sample_point()
			result_dist[int(point)] += 1
		d = Distribution(result_dist, self.x, False)
		d.normalize()
		return d

	def interpolate(self, x):
		g = list()
		for x_k in x:
			j=0
			while j<len(self.x)-2 and x_k>self.x[j+1]: j+=1
			g.append(self.dist_array[j]+(self.dist_array[j+1]-self.dist_array[j])\
			/(self.x[j+1]-self.x[j])*(x_k-self.x[j]))
		return Distribution(g, x)

	def spline_interpolate(self, x, fill_value='extrapolate'):
		if self.dist_array is None:
			self.discretize()
			self.normalize()
		interp = interp1d(self.x, self.dist_array, kind='cubic',
			bounds_error=False, fill_value=fill_value,)
		dist = Distribution(interp(x).tolist(), x)
		dist.discretize()
		return dist

	def length(self):
		return len(self.x)

	''' class methods: get cumulants from moments and v.v.,
		reconstruct distribution from cumulants, interpolate etc. '''
	def cumulants_from_moments(moments):
		m = moments[0]
		m_err = moments[1]
		c = [0] # recursive formula holds for k>0
		c_err = [0]
		order = len(m)
		for n in range(1,order):
			c.append(m[n] - sum([special.binom(n-1,k)*m[k]*c[n-k] for k in range(1,n)]))
			c_err.append(m_err[n] - sum([special.binom(n-1,k)*(m_err[k]*c[n-k]+c_err[n-k]*m[k])
				for k in range(1,n)]))
		return np.array(c), np.array(c_err)

	def moments_from_cumulants(cumulants):
		c = cumulants[0]
		c_err = cumulants[1]
		m = [1]
		m_err = [0]
		order = len(c)
		for n in range(1,order):
			m.append(sum([special.binom(n-1,k)*c[n-k]*m[k] for k in range(n)]))
			m_err.append(sum([special.binom(n-1,k)*(m[k]*c_err[n-k]+c[n-k]*m_err[k]) for k in range(n)]))
		return np.array(m), np.array(m_err)

	def change_moment_range(m, t, s=[-1,1]):
		''' convert moments for a distribution defined in range t
			to the moments for the same distribution resized to range s '''
		moments = m[0]
		errors = m[1]
		l = len(moments)
		new_moments = list()
		new_errors = list()
		a = (s[-1]-s[0])/(t[-1]-t[0])
		b = s[-1]-t[-1]*a
		for k in range(l):
			new_moments.append(sum([special.binom(k,n)*a**(k-n+1)*b**n*moments[k-n]
				for n in range(k+1)]))
			new_errors.append(sum([special.binom(k,n)*a**(k-n+1)*b**n*errors[k-n]
				for n in range(k+1)]))
		return new_moments, new_errors

	def jacobi_recreate(moments, t):
		moments, errors = Distribution.change_moment_range(moments, t)
		t = np.array(t)
		l = len(moments)
		jacobi = [ special.jacobi(i,0,0) for i in range(l) ] # define jacobi polys in range [-1,1]
		for j in range(l):
			jacobi[j] /= math.sqrt(integrate.quad(jacobi[j]**2,-1,1)[0]) # normalize
		coeff = [ # coefficients of the jacobi expansion as combinations of moments
			sum([jacobi[j].c[j-k]*moments[k] for k in range(j+1)])
			for j in range(l) ]
		coeff_err = [ # coefficients of the jacobi expansion as combinations of moments
			sum([jacobi[j].c[j-k]*errors[k] for k in range(j+1)])
			for j in range(l) ]
		dist = sum([coeff[j]*jacobi[j](np.linspace(-1,1,len(t))) for j in range(l)])
		dist_err = sum([coeff_err[j]*jacobi[j](np.linspace(-1,1,len(t))) for j in range(l)])
		d = Distribution(dist, t, error=dist_err)
		return d

	def jacobi_recreate_from_cumulants(cumulants, t):
		return Distribution.jacobi_recreate(Distribution.moments_from_cumulants(cumulants), t)

class Convolution(Distribution):
	def __init__(self, e, f, mc_npoints, disable_bar=False, nstep=200):
		x = np.linspace(np.amin([e.x[0],f.x[0]]), np.amax([e.x[-1],f.x[-1]]), nstep)

		e.discretize()
		e.normalize()
		f.discretize()
		f.normalize()

		e = e.spline_interpolate(x, fill_value=0)
		f = f.spline_interpolate(x, fill_value=0)

		step = (e.x[-1]-e.x[0])/len(e.x)
		y = [ e.x[0]+f.x[0]+j*step for j in range(len(e.x)+len(f.x)) ]
		conv_result = np.array([0]*(e.length()+f.length()))
		error_dist = np.array([0]*(e.length()+f.length()))
		max_e = np.amax(e.dist_array)
		max_f = np.amax(f.dist_array)
		for k in tqdm(range(mc_npoints), disable=disable_bar):
			e_k = c_sample_point(e.dist_array, max_e)
			f_k = c_sample_point(f.dist_array, max_f)
			g_k = int(e_k + f_k)
			conv_result[g_k] += 1
		error_dist = np.sqrt(mc_npoints/(mc_npoints-1)*(conv_result-conv_result**2/mc_npoints))
		Distribution.__init__(self, conv_result, y, False, error=error_dist)
		self.normalize()

class PlotUtil:
	def plot(distributions, title='', xlabel='', ylabel='', legend=None, path=''):
		fig = plt.figure()
		plt.title(title)
		plt.xlabel(xlabel)
		plt.ylabel(ylabel)
		if legend is None: legend = ['']*len(distributions)
		handles = list()
		for dist in distributions:
			if dist.dist_array is None: dist.discretize()
			handles.append(plt.plot(dist.x, dist.dist_array, label=legend.pop(0))[0])
		plt.legend(handles=handles)
		if path!='': fig.savefig(path, dpi=300)
		plt.show()
		plt.close(fig)