import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from shower_utils import *
import convolution_utils
from convolution_utils import Distribution, Convolution

nstep = 200
times = numpy.linspace(convolution_utils.times[0], convolution_utils.times[-1], nstep, endpoint=False)
x = np.linspace(0,8,nstep)
y = x*(1/c-1/v)
h = lambda y: shower_func(y/(1/c-1/v))
hlist = list(map(h,y))

y1 = times+L/v
r = interp1d(convolution_utils.times, convolution_utils.spr)
w = lambda y: r(y-L/v)

light_dist = Distribution(y, hlist)
light_dist.discretize()
light_dist.normalize()
spr_dist = Distribution(y1, w(y1))
spr_dist.discretize()
spr_dist.normalize()

d = Convolution(spr_dist, light_dist, 10000)