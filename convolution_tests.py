#!/usr/bin/python3

import math, sys, os
import matplotlib.pyplot as plt
from tqdm import tqdm

import numpy as np

import convolution_utils
from convolution_utils import Convolution, Distribution

plot_path = 'graphs/convolution_tests/'
if not os.path.exists(plot_path): os.makedirs(plot_path)

if len(sys.argv)>1: mc_npoints = int(sys.argv[1])
else: mc_npoints = 100000
if len(sys.argv)>2: mc_step = int(sys.argv[2])
else: mc_step = 10000

class UtilDistributions:
	def gaussian(x, mean, sigma):
		return 1/(sigma*math.sqrt(2*math.pi))*math.exp(-(x-mean)**2/(2*sigma**2))
	def g1(x): return UtilDistributions.gaussian(x, 1, 1)
	def g2(x): return UtilDistributions.gaussian(x, 2, 1)
	def g1g2(x): return conv(x, g1, g2, 1-20, 1+20, 2-20, 2+20, npoints)

	def exponential(x, mean):
		if x>0:	return mean*math.exp(-mean*x)
		else: return 0
	def e1(x): return UtilDistributions.exponential(x, 1)
	def e2(x): return UtilDistributions.exponential(x, 2)
	def e1e2(x): return conv(x, e1, e2, 0, 20, 0, 20, npoints)

	def uniform(x, min, max):
		if x<=max and x>=min: return 1/(max-min)
		else: return 0
	def u1(x): return UtilDistributions.uniform(x, -1, 1)
	def u2(x): return UtilDistributions.uniform(x, -2, 2)
	def u1u2(x): return conv(x, u1, u2, -5, 5, -5, 5, npoints)

	def laplace(x, mean):
		return mean/2*math.exp(-mean*abs(x))
	def l1(x): return UtilDistributions.laplace(x, 1)
	def l2(x): return UtilDistributions.laplace(x, 2)
	def l1l2(x): return conv(x, l1, l2, -5, 5, -5, 5, npoints)

	def util_dist_list():
		return [
			(UtilDistributions.g1, UtilDistributions.g2, 0.05, range(-200, 200), 'Gaussian'),
			(UtilDistributions.e1, UtilDistributions.e2, 0.05, range(0, 200), 'Exponential'),
			(UtilDistributions.u1, UtilDistributions.u2, 0.05, range(-200, 200), 'Uniform'),
			(UtilDistributions.l1, UtilDistributions.l2, 0.05, range(-200, 200), 'Laplace')
		]

matrix_list = list()
fmatrix = list()
k = 0
for el in tqdm(UtilDistributions.util_dist_list()):
	(f1, f2, step, rng, title) = el
	x = [ step*i for i in rng ]
	func1 = Distribution(f1, x)
	func1.discretize()
	func1.normalize()
	func2 = Distribution(f2, x)
	func2.discretize()
	func2.normalize()
	cdiff_matrix = list()
	cdiff_relative_matrix = list()
	npoints_list = list()
	cumulants_matrix1 = list()
	cumulants_matrix2 = list()
	cumulants_matrix3 = list()
	fmatrix.append(list())
	for npoints in tqdm(range(100, mc_npoints, mc_step)):
		fmatrix[k].append(Convolution(func1, func2, npoints, disable_bar=True))
		if npoints==mc_npoints-step: fmatrix[-1][-1].plot(show=True)
		cumulants_matrix1.append(func1.cumulants()[1:])
		cumulants_matrix2.append(func2.cumulants()[1:])
		cumulants_matrix3.append(fmatrix[-1][-1].cumulants()[1:])
		cdiff_matrix.append(cumulants_matrix1[-1]+cumulants_matrix2[-1]-cumulants_matrix3[-1])
		cdiff_relative_matrix.append(cdiff_matrix[-1]/cumulants_matrix3[-1])
		cumulants1 = cumulants_matrix1[-1]
		cumulants2 = cumulants_matrix2[-1]
		cumulants3 = cumulants_matrix3[-1]
		cdiff = cdiff_matrix[-1]
		cdiff_relative = cdiff_relative_matrix[-1]
		'''cdiff_matrix.append([(cumulants1[k]+cumulants2[k]-cumulants3[k])/cumulants3[k]
			if cumulants3[k]!=0 else 0
			for k in range(len(cumulants1))])'''
		#print(cumulants1, cumulants2, cumulants3, cdiff, '\n\n', sep='\n')
		npoints_list.append(npoints)
	#cdiff_matrix = convolution_utils.transpose_matrix(cdiff_matrix)
	matrix_list.append(dict(
		title = title,
		cdiff_matrix = cdiff_matrix,
		cdiff_relative_matrix = cdiff_relative_matrix,		
		npoints_list = npoints_list,
		tmp = (cumulants_matrix1, cumulants_matrix2, cumulants_matrix3)
	))
	k += 1
	#for i in range(len(cdiff_matrix)):
	#	l = cdiff_matrix[i]
	#	fig = plt.figure()
	#	plt.xlabel('# samples')
	#	plt.ylabel('discrepancy')
	#	plt.title(title+' order '+str(i))
	#	plt.plot(npoints_list, l)
	#	fig.savefig(plot_path+title+'_'+str(i)+'.png',dpi=200)
	#	plt.close(fig)