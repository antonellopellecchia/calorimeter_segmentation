#!/usr/bin/python3

import sys
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
from shower_utils import *
import convolution_utils
from convolution_utils import Distribution, Convolution

helpstr = 'Usage: pmt_signal.py mc_ncpoints [--plot] [--save] [--order int]'
if len(sys.argv)<2:
	print(helpstr)
	sys.exit(0)
mc_npoints = int(sys.argv[1])
plot = False
save = False
order = 9
for i in range(len(sys.argv)):
	arg = sys.argv[i]
	if arg == '--plot': plot=True
	if arg == '--save': save=True
	if arg == '--order':
		order = int(sys.argv[i+1])
	if arg == '--help':
		print(helpstr)
		sys.exit(0)

plot_path = 'graphs/pmt_signal/'
nstep = 200

times = numpy.linspace(convolution_utils.times[0], convolution_utils.times[-1], nstep, endpoint=False)
x = np.linspace(0,8,nstep)
y = x*(1/c-1/v)
y = np.flip(y,0)
h = lambda y: shower_func(y/(1/c-1/v))
hlist = list(map(h,y))

sho_arr = list(map(shower_func,x))
sho = Distribution(sho_arr, x)
sho.normalize()
if plot:
	fig, ax = plt.subplots()
	ax.plot(sho.x, sho.dist_array)
	ax.set_title('Longitudinal shower shape')
	ax.set_xlabel('Depth ($X_0$)')
	if save: fig.savefig(plot_path+'light_dist.png', dpi=300)
	plt.show()
	plt.close(fig)

	fig, ax = plt.subplots()
	ax.plot(times, list(map(shower_func_t, times)))
	ax.set_title('Light distribution at the PMT surface')
	ax.set_xlabel('Time (s)')
	if save: fig.savefig(plot_path+'light_dist_t.png', dpi=300)
	plt.show()
	plt.close(fig)

r = interp1d(convolution_utils.times, convolution_utils.spr, fill_value='extrapolate')
if plot:
	fig, ax = plt.subplots()
	ax.plot(times, r(times))
	ax.set_title('Single photon response')
	ax.set_xlabel('Time (s)')
	if save: fig.savefig(plot_path+'spr_pmt.png', dpi=300)
	plt.show()
	plt.close(fig)

y1 = times+L/v
w = lambda y: r(y-L/v)


domain = np.linspace(np.amin([y[0],y1[0]]), np.amax([y[-1],y1[-1]]), 200)

light_dist = Distribution(hlist, y)
light_dist.discretize()
light_dist.normalize()
light_dist = light_dist.spline_interpolate(domain, fill_value=0)
spr_dist = Distribution(w(y1), y1)
spr_dist.discretize()
spr_dist.normalize()
spr_dist = spr_dist.spline_interpolate(domain, fill_value=0)

result = Convolution(spr_dist, light_dist, mc_npoints)
result.normalize()


t = np.linspace(2*times[0],2*times[-1],400)
if plot:
	fig = plt.figure()
	flipdist = np.flip(result.dist_array, 0)
	flipdist_err = np.flip(result.error, 0)
	plt.plot(t, flipdist)
	plt.fill_between(t, flipdist-flipdist_err, flipdist+flipdist_err, color='gray', alpha=0.2)
	plt.title('PMT signal (convolution)')
	plt.xlabel('Time (s)')
	if save: fig.savefig(plot_path+'pmt.png', dpi=300)

spr_cumulants, spr_cumulants_err = spr_dist.cumulants(order)
light_cumulants, light_cumulants_err = light_dist.cumulants(order)
result_cumulants, result_cumulants_err = result.cumulants(order)
calc_light_cumulants = [result_cumulants[k]-spr_cumulants[k] for k in range(order)]
calc_light_cumulants_err = [result_cumulants_err[k]+spr_cumulants_err[k] for k in range(order)]

deconvolution = Distribution.jacobi_recreate_from_cumulants(
	[calc_light_cumulants, calc_light_cumulants_err], y)
deconvolution.normalize()

cumulants_discrepancies = np.array([(calc_light_cumulants[k]-light_cumulants[k])/light_cumulants[k]
	if light_cumulants[k]!=0 else 0 for k in range(order)])
cumulants_discrepancies_err = np.array([calc_light_cumulants_err[k]/light_cumulants[k]
	if light_cumulants[k]!=0 else 0 for k in range(order)])

if plot:
	fig = plt.figure()
	orders = list(range(len(cumulants_discrepancies)))
	plt.plot(orders, cumulants_discrepancies)
	plt.fill_between(orders,
		cumulants_discrepancies-cumulants_discrepancies_err,
		cumulants_discrepancies+cumulants_discrepancies_err,
		color='gray', alpha=0.2)
	plt.title('Cumulant discrepancy')
	plt.xlabel('Cumulant order')
	plt.ylabel('Relative discrepancy')
	if save: fig.savefig(plot_path+'pmt_discrepancies.png',dpi=300)


x = np.flip(np.array(deconvolution.x)/(1/c-1/v),0)
h = interp1d(deconvolution.x, deconvolution.dist_array)
g = lambda z: h(z*(1/c-1/v))
glist = list(map(g,x))
shower = Distribution(glist, x, error=deconvolution.error)
shower.normalize()

sho_arr = list(map(shower_func,x))
sho = Distribution(sho_arr, x)
sho.normalize()

if plot:
	fig = plt.figure()
	plt.plot(sho.x, sho.dist_array)
	plt.plot(shower.x, shower.dist_array)
	plt.fill_between(shower.x,
		shower.dist_array-shower.error,
		shower.dist_array+shower.error,
		color='gray', alpha=0.2)
	plt.title('Shower development comparison')
	plt.xlabel('Depth ($X_0$)')
	plt.legend(['Original shape', 'Recreated'])
	plt.show()
	if save: fig.savefig(plot_path+'light_comparison.png',dpi=300)


diff = shower - sho
if plot:
	fig = plt.figure()
	plt.plot(diff.x, diff.dist_array)
	plt.title('Reconstructed distribution discrepancy')
	plt.xlabel('Depth ($X_0$)')
	plt.ylabel('Relative discrepancy')
	if save: fig.savefig(plot_path+'recreated_discrepancies.png',dpi=300)